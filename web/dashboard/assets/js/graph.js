
function data(year){
    let Data = [];
    $.ajax({
    url: 'http://gardenyard.8u.cz/src/content/contentprint.php?graph='+year,
    async: false,
    method: "GET",
    success: function(response){
        Data = JSON.parse(response);
    }   
    });
    return Data;
}

function Box (year){
    $.ajax({
        url: 'http://gardenyard.8u.cz/src/content/contentprint.php?year='+year,
        async: false,
        method: 'GET',
        success: function(response){
            Data = JSON.parse(response);
        }              
    });

    $('#price').html(Data[1]);
    $('#count').html(Data[0]);
}

function graphApi(year){

 $('.chartjs-size-monitor').remove();

  const Months = [ "Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"];
  const Data = data(year);

  let Year = year; 

  const ctx = document.getElementById('graphAPI').getContext('2d');
  const myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: Months,
        datasets: [{
            label: 'Počet zákazek v roce '+Year,
            data: Data,
            backgroundColor: [
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 206, 86, 1)'
            ],
            borderColor: [

            ],
            borderWidth: 2
        }]},
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                  }
                }]
            }
        }
    });
}

$("button[name='yearChanger']").click(function(){
    let year = $("select").children("option:selected").val();
    graphApi(year);
    Box(year);
});