$('input[name=password_check]').focus(function() {
  $('.Validator').show();  // Show validation conditions
});

$('input[name=password_check]').blur(function() {
  $('.Validator').hide(); // Hide validation conditions
});

function Check_PSW(){
  let PasswordInput = document.getElementsByName('password'); // Get input to check password
  let CheckerInput = document.getElementsByName('password_check'); // Input for checking 

  let Password = PasswordInput[0].value; // Get input value 
  let Checker = CheckerInput[0].value; // same 

  if (Password == Checker) {
    $('#check').removeClass('bx-x'); 
    $('#check').addClass('bx-check'); // Change icon from x to ✅
  }
  else {
    $('#check').removeClass('bx-check');
    $('#check').addClass('bx-x'); // Reset validation table
  }
}