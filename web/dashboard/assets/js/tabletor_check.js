let form = document.getElementById('contract');
let ConID;
const ResultsAmount = 10;
if (form != undefined) { Changer(0, 1);}

function Changer(status, Page) {
  let queryString = window.location.search;
  let urlParams = new URLSearchParams(queryString);
  let product = urlParams.get('edit');

  ConID = product.replace("Co", "");
  const Addres = "http://gardenyard.8u.cz/src/content/contentprint.php?contract=";
  let Data = serverCall(Addres, ConID);
  let PData = Pagation(Data, Page);
  if (status == 0) {
    tableCreator (0, form, PData, ConID, Page);
  }
  else {
    tableCreator (1, form, PData, ConID, Page);
  }
}

function serverCall(Addres, ID) {
  let Server = "";
  $.ajax({
    url: Addres+ID,
    async: false,
    method: "GET",
    success: function(response){
      Server = JSON.parse(response);
    }  
  });
  return Server;
}

function Pagation(Data, Page) {
  let NewData = [];
  let Materials = Data[0];
  let Works = Data[1];

  const DividedMaterials = new Array(Math.ceil(Materials.length / ResultsAmount))
  .fill()
  .map(_ => Materials.splice(0, ResultsAmount));

  const DividedWorks = new Array(Math.ceil(Works.length / ResultsAmount))
  .fill()
  .map(_ => Works.splice(0, ResultsAmount));

  NewData = [DividedMaterials[Page-1], DividedWorks[Page-1]];

  return NewData;
}

function tableCreator(type, form, Data, ID, Page) {
  let PageM = Page-1;
  let PageP = Page+1;

  $('table').remove();
  $('.swiper').remove();

  if (type == 0) {
    Modul =  Data[0]; 
    Links = ["#", "Skupina","Popis", "Cena/kus", "Počet"];
  }
  else {
    Modul =  Data[1]; 
    Links = ["#", "Popis","Cena/h", "Čas (h)"];
  }

  // Create a elements
  let table = document.createElement("table");
  let tableHeader = document.createElement("thead");
  let tableBody = document.createElement("tbody");
  let headerRow = document.createElement('tr'); 

  // Function for create a Table Header
  Links.forEach(headerText => {
    let header = document.createElement('th');
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableHeader.appendChild(headerRow);
  table.appendChild(tableHeader);

  // Function for create a Table Body

  if (Modul == undefined) {
    PageP = Page-2;
    PageP = Page-1;
    Changer(type, Page-1);
    $('.swiper').remove();
  }
  else {
    Modul.forEach(value => {
      let row = document.createElement('tr');
      let index = 0;
      Object.values(value).forEach(text => {
        let cell = document.createElement('td');

        if (type == 0) {
          let input = document.createElement('input');
            
          if (index == 4) {
            input.type = 'number';
            input.value = value.Amount;
            input.name = ''+ value.ID +'';
            input.min="0";
            input.setAttribute("onchange", "Calc(this,'"+ value.Price +"', 'M')");
            cell.setAttribute("data-label", Links[index]);
            cell.appendChild(input);
            row.appendChild(cell);
            index++;
          }
          else{
            let textNode = document.createTextNode(text);
            cell.appendChild(textNode);
            cell.setAttribute("data-label", Links[index]);
            row.appendChild(cell);
            index++;
          }
        }
        else if(type == 1) {
            let input = document.createElement('input');
             
            if (index == 3) {
              input.type = 'number';
              input.value = value.Amount;
              input.setAttribute("onchange", "Calc(this,'"+ value.Rate +"', 'W')");
              input.name = ''+ value.ID +'';
              cell.appendChild(input);
              cell.setAttribute("data-label", Links[index]);
              row.appendChild(cell);
              index++; 
            }
            else{
              let textNode = document.createTextNode(text);
              cell.appendChild(textNode);
              cell.setAttribute("data-label", Links[index]);
              row.appendChild(cell);
              index++;
            } 
          }
      });
      tableBody.appendChild(row);
    });
    table.appendChild(tableBody);

    form.appendChild(table);
   
    if (PageM == 0) {
      PageM = 1;
    }
    PageP = Page + 1;
  }
  
  //Create Swiper 

  let MinusB =  document.createElement('button');
  MinusB.innerText = '«';
  MinusB.setAttribute("onclick", "Changer("+type+", "+PageM+")");
  MinusB.setAttribute("class", "swiper"); 

  let PlusB =  document.createElement('button');
  PlusB.innerText = '»';
  PlusB.setAttribute("onclick", "Changer("+type+", "+PageP+")");
  PlusB.setAttribute("class", "swiper");

  form.appendChild(MinusB);
  form.appendChild(PlusB);
}
