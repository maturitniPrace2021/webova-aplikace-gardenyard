$("#leftside-navigation .sub-menu > a").click(function(e) {
  $("#leftside-navigation ul ul").slideUp(), $(this).next().is(":visible") || $(this).next().slideDown(),
  e.stopPropagation()
})

$('#responsivebtn').click(function(e) {
    let button = $('#responsivebtn');
    if (button.hasClass("none") == false) {
      $('#responsivebtn').addClass("none");
      $('.sidebar').css({ "display": 'none'});
    }
    else{
      $('#responsivebtn').removeClass("none");
      $('.sidebar').css({ "display": 'block'});
    }
  }
);

$(window).resize(function() {
  if ( $(window).width() > 1200 && $('.sidebar').css('display') == 'none') {
    $('.sidebar').css({ "display": 'block'});
  }
});