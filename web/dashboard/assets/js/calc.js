let Amounts, ID;

let Input = document.getElementsByName('Price');
if (Input) { Counter();}

function serverCall(URL) {
  let Server = "";
  $.ajax({
    url: URL,
    async: false,
    method: "GET",
    success: function(response){
      Server = JSON.parse(response);
    }  
  });
  return Server;
}

function Counter() {
  let queryString = window.location.search;
  let urlParams = new URLSearchParams(queryString);
  let product = urlParams.get('edit');
  ID = product.replace("Co", "");
  
  let URL = "http://gardenyard.8u.cz/src/content/contentprint.php?contract=";
  
  Amounts = serverCall(URL+ID);
}

function sendAmount(URL) {
  $.ajax({
    url: URL,
    async: false,
    method: "GET",
  });
}

function Calc (target, Quantity, Type) {

  let PriceInput = document.getElementsByName('Price'); // Price Input
  let Price = Number(PriceInput[0].value); // Get value of price input
  let newPrice, URL;

  let newAmount = Number(target.value); // New amount
  let MatID = target.name; // New amount ID
  let removeCurrency = Quantity.replace(/[^0-9+,.-]+/g,""); // Remove Kč atribut 
  let Currency = Number(removeCurrency.replace(',', '.')); // Replace from , to .

  if (Type == 'M') {
    URL = "http://localhost/garden-yard/src/database_Api/databaseApi.php?ConID="+ID+"&MatID="+MatID+"&Amount="+newAmount;
    Amounts[0].forEach(product => {
      if (product.ID == MatID) {
        removeAmount = Price - (Currency*product.Amount);
        newPrice = removeAmount + (Currency*newAmount);
        product.Amount = newAmount;
      }
    });
  }
  else {
    URL = "http://localhost/garden-yard/src/database_Api/databaseApi.php?ConID="+ID+"&WorkID="+MatID+"&Amount="+newAmount;
    Amounts[1].forEach(product => {
      if (product.ID == MatID) {
        removeAmount = Price - (Currency*product.Amount);
        newPrice = removeAmount + (Currency*newAmount);
        product.Amount = newAmount;
      }
    });
  }

  sendAmount(URL);
  PriceInput[0].value = newPrice;
}