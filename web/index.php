<!DOCTYPE html>
<html lang="cz">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GardenYard - Login</title>
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/util.css">
  </head>
  <body>
    <div class='main'>
      <div class='container'>
        <div class='login-wrap'>
          <form method='post' action='index.php'>
            <div class='login-tile p-b-48'>
              <img src="assets/img/logo.svg" alt="logo aplikace">
            </div>      
            <div class='input-container'>
              <label for="login-name">Login</label>
              <input class='input100' type="text" name='login-name'>
            </div>
            <div class='input-container m-b-50'>
              <label for="login-pswd">Heslo</label>
              <input class='input100' type="password" name='login-pswd' place-holder='Heslo..'>
            </div>
            <div class='button-container'>
              <button> Příhlásit se </button>
            </div>
        </form>  
        </div>
      </div>
    </div>
    <?php require_once('src/connection/login.php'); ?>
  </body>
</html>