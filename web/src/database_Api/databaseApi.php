<?php 

  //Define atribut for connetion to Database 
  
  define ("SERVERNAME"," ");
  define ("USERNAME"," ");   
  define ("PASSWORD"," ");
  define ("NAME"," ");

  function Send($Sql){ 
    $Data = []; // Return data
    
    $conn = new mysqli(SERVERNAME, USERNAME, PASSWORD, NAME); // Database connection
    
    if ($conn->connect_error) { // Fatal connection protect
      die("Connection failed: " . $conn->connect_error); 
    }
    
    $result = $conn->query($Sql); // Push Sql command

    if ( isset($result->num_rows) && $result->num_rows > 0) {
      while($row = $result->fetch_assoc()) { 
        $Data[] = $row; // Get value from database
      }
    }

    if(isset($Data)){return($Data);}
  }

  function Push($Sql_Command){
  
    $conn = new mysqli(SERVERNAME, USERNAME, PASSWORD, NAME); // Database connection
    
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }
  
    $Sql = $Sql_Command;
      
    $Push = $conn->query($Sql);
  }

 //Update functions 

function updateUser($ID, $UserName,$LastName,$FirstName,$Rank){
  $ID = '"'.$ID.'"';
  $UserName = '"'.$UserName.'"';
  $LastName = '"'.$LastName.'"';
  $FirstName = '"'.$FirstName.'"';
  //$Password = '"'.sha1($Password).'"';
  $Rank = '"'.$Rank.'"';


  $Sql_Command = "UPDATE `Users` SET `UserName`=$UserName,`LastName`=$LastName,`FirstName`=$FirstName, `Rank`=$Rank WHERE ID = $ID"; 

  Push($Sql_Command);
}

function updateContract($CusID, $UserID,$Day, $ID, $Price, $Status){
  $ID = '"'.$ID.'"';
  $CusID = '"'.$CusID.'"';
  $UserID = '"'.$UserID.'"';
  $Day = '"'.$Day.'"';
  $Price = '"'.$Price.'"';

  $Sql_Command = "UPDATE `Contract` SET `CusID`=$CusID,`UserID`=$UserID,`Day`=$Day, Price=$Price, Status=$Status WHERE ID = $ID";

  Push($Sql_Command);
}

function updateCustomer($ID,$FirstName, $LastName,$Tel,$Email,$Address){
  $ID = '"'.$ID.'"';
  $FirstName = '"'.$FirstName.'"';
  $LastName = '"'.$LastName.'"';
  $Tel = '"'.$Tel.'"';
  $Email = '"'.$Email.'"';
  $Address = '"'.$Address.'"';

  $Sql_Command = "UPDATE `Customers` SET `FirstName`=$FirstName,`LastName`=$LastName,`Tel`=$Tel,`Email`=$Email,`Address`=$Address WHERE ID = $ID";
  
  Push($Sql_Command);
}

function updateMaterial($ID,$Index, $Description,$Price){
  $ID = '"'.$ID.'"';
  $Index = '"'.$Index.'"';
  $Description = '"'.$Description.'"';
  $Price = '"'.$Price.'"';

  $Sql_Command = "UPDATE `Materials` SET `Index`=$Index,`Description`=$Description,`Price`=$Price WHERE ID = $ID";

  Push($Sql_Command);
}

function updateWorks($ID,$Type, $Rate){
  $ID = '"'.$ID.'"';
  $Type = '"'.$Type.'"';
  $Rate = '"'.$Rate.'"';

  $Sql_Command = "UPDATE `Work` SET `Type`=$Type,`Rate`=$Rate WHERE ID = $ID";

  Push($Sql_Command);
}

function updateMaterialList($ConID, $MatID, $Amount){
  $ConID = '"'.$ConID.'"';
  $MatID = '"'.$MatID.'"';
  $Amount = '"'.$Amount.'"';

  $Sql_Command = "UPDATE `MaterialList` SET `Amount`= $Amount WHERE ConID = $ConID AND MatID = $MatID";
  Push($Sql_Command);
}

function updateWorkList($ConID, $WorkID, $Amount){
  $ConID = '"'.$ConID.'"';
  $WorkID = '"'.$WorkID.'"';
  $Amount = '"'.$Amount.'"';

  $Sql_Command = "UPDATE `MaterialList` SET `Amount`= $Amount WHERE ConID = $ConID AND WorkID = $WorkID";
  Push($Sql_Command);
}

//Delete functions 

function deleteUser($ID){

  $Sql_Command = "DELETE FROM `Users` WHERE ID = $ID";

  Push($Sql_Command);
}

function deleteContract($ID){

  $Sql_Command = "DELETE FROM `Contract` WHERE ID = $ID";

  Push($Sql_Command);
}

function deleteCustomer($ID){

  $Sql_Command = "DELETE FROM `Customers` WHERE ID = $ID";

  Push($Sql_Command);
}

function deleteMaterialsList($ID, $MatID){

  $Sql_Command = "DELETE FROM `MaterialList` WHERE ConID = $ID AND MatID = $MatID";
  
  Push($Sql_Command);
}

function deleteMaterial($ID){

  $Sql_Command = "DELETE FROM `Materials` WHERE ID = $ID";

  Push($Sql_Command);
}

function deleteWork($ID){

  $Sql_Command = "DELETE FROM `Work` WHERE ID = $ID";

  Push($Sql_Command);
}

function deleteWorkList($ConID, $WorkID){

  $Sql_Command = "DELETE FROM `WorkList` WHERE ConID = $ConID AND WorkID = $WorkID";

  Push($Sql_Command);
}

//Insert Into functions

function insertUser($UserName,$LastName,$FirtName,$Password,$Rank){
  $UserName = '"'.$UserName.'"';
  $LastName = '"'.$LastName.'"';
  $FirtName = '"'.$FirtName.'"';
  $Pwd = '"'.sha1($Password).'"';;
  $Rank = '"'.$Rank.'"';
  

  $Sql_Command = "INSERT INTO `Users` (`UserName`, `LastName`, `FirstName`, `Password`, `Rank`) VALUES ($UserName, $LastName, $FirtName, $Pwd, $Rank)";

  Push($Sql_Command);
}

function  insertContract($CusID, $UserID,$Day){
  $CusID = '"'.$CusID.'"';
  $UserID = '"'.$UserID.'"';
  $Day = '"'.$Day.'"';

  $Sql_Command = "INSERT INTO `Contract`(`CusID`, `UserID`, `Day`) VALUES ($CusID,$UserID,$Day)"; 

  Push($Sql_Command);
}

function  insertCustomer($FirstName, $LastName,$Tel,$Email,$Address){
  $FirstName = '"'.$FirstName.'"';
  $LastName = '"'.$LastName.'"';
  $Tel = '"'.$Tel.'"';
  $Email = '"'.$Email.'"';
  $Address = '"'.$Address.'"';

  $Sql_Command = "INSERT INTO `Customers`(`FirstName`, `LastName`, `Tel`, `Email`, `Address`) VALUES ($FirstName, $LastName, $Tel, $Email, $Address)";

  Push($Sql_Command);
}

function  insertMaterial($Index, $Description,$Price){
  $Index = '"'.$Index.'"';
  $Description = '"'.$Description.'"';
  $Price = '"'.$Price.'"';


  $Sql_Command = "INSERT INTO `Materials`(`Index`, `Description`, `Price`) VALUES ($Index, $Description,$Price)";

  Push($Sql_Command);
}

function  insertMaterialList($ConID,$MatID, $Amount){
  $ConID = '"'.$ConID.'"';
  $MatID = '"'.$MatID.'"';
  $Amount = '"'.$Amount.'"';

  $Sql_Command = "INSERT INTO `MaterialList`(`ConID`, `MatID`, `Amount`) VALUES ($ConID, $MatID, $Amount)";

  Push($Sql_Command);
}

function  insertWorks($Type, $Rate){
  $Type = '"'.$Type.'"';
  $Rate = '"'.$Rate.'"';

  $Sql_Command = "INSERT INTO `Work`(`Type`, `Rate`) VALUES ($Type,$Rate)";

  Push($Sql_Command);
}

function  insertWorksList($ConID, $WorkID,$Amount){
  $ConID = '"'.$ConID.'"';
  $WorkID = '"'.$WorkID.'"';
  $Amount = '"'.$Amount.'"';

  $Sql_Command = "INSERT INTO `WorkList`(`ConID`, `WorkID`, `Amount`) VALUES ($ConID, $WorkID, $Amount)";

  Push($Sql_Command);
}

if (isset($_GET["ConID"]) && isset($_GET["MatID"]) && isset($_GET["Amount"])) {
 $ConID = $_GET["ConID"];
 $MatID = $_GET["MatID"];
 $Amount = $_GET["Amount"];

  if ($Amount == 0) {
    deleteMaterialsList($ConID, $MatID);
  }
  else {
    $SQL = "SELECT MatID FROM MaterialList WHERE ConID=$ConID AND MatID=$MatID";
    $Check = Send($SQL);
    if (empty($Check)) {
      insertMaterialList($ConID,$MatID, $Amount);
    }
    else {
      updateMaterialList($ConID, $MatID, $Amount);
    }
  }
}

if (isset($_GET["ConID"]) && isset($_GET["WorkID"]) && isset($_GET["Amount"])) {
  $ConID = $_GET["ConID"];
  $WorkID = $_GET["WorkID"];
  $Amount = $_GET["Amount"];
 
   if ($Amount == 0) {
    deleteWorkList($ConID, $WorkID);
   }
   else {
     $SQL = "SELECT WorkID FROM WorkList WHERE ConID=$ConID AND WorkID=$WorkID";
     $Check = Send($SQL);
     if (empty($Check)) {
      insertWorksList($ConID, $WorkID,$Amount);
     }
     else {
      updateWorkList($ConID, $WorkID, $Amount);
     }
   }
 }
?>
