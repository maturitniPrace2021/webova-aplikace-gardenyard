<?php 
  // Function: Creating tables

  function Tablator($Parametr, $Data, $Page){

    $MinusPage = $Page-1;
    if($MinusPage == 0){ $MinusPage=1;}

    $PlusPage = $Page+1;

    if (isset($Data[0])) {
      $Table_Header = array_keys($Data[0]);
      $Table_Header[0] = '#';

      echo ('<div id="table" class="contr-table">');
        echo ('<table>');

          echo ('<thead>');
            echo ('<tr>');
              foreach ($Table_Header as $Link) {
                echo ('<th>'.$Link.'</th>');
              }
            echo ('</tr>');
          echo ('</thead>');

          echo ('<tbody>');
            foreach ($Data as $Link) {
              echo ('<tr>');
                $Defi = array_values($Link);
                for ($i=0; $i < count($Table_Header); $i++) { 
                  echo ('<td data-label="'.$Table_Header[$i].'">');
                    echo ('<a href="index.php?edit='.$Parametr.$Defi[0].'">'.$Defi[$i].'</a>');
                  echo ('</td>');
                }
              echo ('</tr>'); 
            }
          echo ('</tbody>');
        echo ('</table>');
        
        echo ('<div>');
          echo ('<a href="index.php?content='.$_GET["content"].'&type='.$_GET["type"].'&page='.$MinusPage.'">«</a>');
          echo ('<a href="index.php?content='.$_GET["content"].'&type='.$_GET["type"].'&page='.$PlusPage.'">»</a>');
        echo ('</div></div>');
    }
    else {
      $RPage = $Page -1;
      print_r ($Data);

      $URL = $_SERVER['REQUEST_URI'];
      $Reset_URL = str_replace("page=$Page", "page=$RPage", $URL);

      header("Location:http://gardenyard.8u.cz".$Reset_URL);
    }
  }

  function TablatorC($Parametr, $Data, $Page, $Filtr){

    $MinusPage = $Page-1;
    if($MinusPage == 0){ $MinusPage=1;}

    $PlusPage = $Page+1;

    if (isset($Data[0])) {
      $Table_Header = array_keys($Data[0]);
      $Table_Header[0] = '#';

      echo ('<div id="table">');
        echo ('<div class="btn-filtr">');
          echo ('<a href="index.php?content='.$_GET["content"].'&type='.$_GET["type"].'&page=1&filtr=0">Rozpracované</a>');
          echo ('<a href="index.php?content='.$_GET["content"].'&type='.$_GET["type"].'&page=1&filtr=1">Hotové</a>');
        echo ('</div>');
  
        echo ('<table>');

          echo ('<thead>');
            echo ('<tr>');
              foreach ($Table_Header as $Link) {
                echo ('<th>'.$Link.'</th>');
              }
            echo ('</tr>');
          echo ('</thead>');

          echo ('<tbody>');
            foreach ($Data as $Link) {
              echo ('<tr>');
                $Defi = array_values($Link);
                for ($i=0; $i < count($Table_Header); $i++) { 
                  echo ('<td data-label="'.$Table_Header[$i].'">');
                    echo ('<a href="index.php?edit='.$Parametr.$Defi[0].'">'.$Defi[$i].'</a>');
                  echo ('</td>');
                }
              echo ('</tr>'); 
            }
          echo ('</tbody>');
        echo ('</table>');
        
        echo ('<div>');
          echo ('<a href="index.php?content='.$_GET["content"].'&type='.$_GET["type"].'&page='.$MinusPage.'&filtr='.$Filtr.'">«</a>');
          echo ('<a href="index.php?content='.$_GET["content"].'&type='.$_GET["type"].'&page='.$PlusPage.'&filtr='.$Filtr.'">»</a>');
        echo ('</div></div>');
    }
    else {
      if($Page == 1) {
        echo ('<div class="Error">');
          echo ('<p>Nejsou nalazené žádné zákazky</p>');
        echo ('</div>');
      }
      else {
        $RPage = $Page -1;
        print_r ($Data);
  
        $URL = $_SERVER['REQUEST_URI'];
        $Reset_URL = str_replace("page=$Page", "page=$RPage", $URL);
  
        header("Location:http://gardenyard.8u.cz".$Reset_URL);
      }
    }
  }

   // Function: Forms editors 
        
  function InputCreator($Data){
    $Names =  array_keys($Data);
    $Values = array_values($Data);

    for ($i=0; $i < count($Names);) { 
      for ($x=0; $x < count($Values);) { 
        if ($i == 0) {
          echo ('<input name="'.$Names[$i].'" value="'.$Values[$x].'" placeholder="'.$Names[$i].'..." READONLY>');
          echo ("<br>");
          $x++;
          $i++;
        }
        else{
          echo ('<input name="'.$Names[$i].'" value="'.$Values[$x].'" placeholder="'.$Names[$i].'...">');
          echo ("<br>");
          $x++;
          $i++;
        }
      }
    }
  }

  function Formator( $Values, $Parametr, $ID){
    
    if ($Parametr == "Cu") {
      echo ('<form action="index.php" method="post">');
        InputCreator($Values);
        echo '<button type="submit" name="customersUpdate">Poslat</button>';
        echo '<button name="customerDelete" value='.$ID.' type="submit">Smazat</button>';
      echo "</form>";
    }
    
    if ($Parametr == "Us") {
      echo ('<form action="index.php?edit=Us'.$ID.'" method="post">');
        InputCreator($Values);
        echo '<button type="submit" name="usersUpdate">Poslat</button>';
        echo '<button name="userDelete" value='.$ID.' type="submit">Smazat</button>';
      echo "</form>";
    }
    
    if ($Parametr == "Ma") {
      echo ('<form action="index.php" method="post">');
        InputCreator($Values);
        echo '<button type="submit" name="materialsUpdate">Poslat</button>';
        echo '<button name="materialDelete" value='.$ID.' type="submit">Smazat</button>';
      echo "</form>";
    }

    if ($Parametr == "Wo") {
      echo ('<form action="index.php" method="post">');
        InputCreator($Values);
        echo '<button type="submit" name="workUpdate">Poslat</button>';
        echo '<button name="workDelete" value='.$ID.' type="submit">Smazat</button>';
      echo "</form>";
    }
  }

   // Editor section

  // Function: Singpost forms for all categories

  function Editor($type){ 
    
    $Type = str_split($type); // Word to array

    $Parametr = "$type[0]$type[1]"; // Get from array paramert of table 

    $ID = array_splice( $Type,2); // remove paramater and get id 
    $ID = implode($ID); // array to string 

    switch ($Parametr){
      case 'Us':
        $SQl = "SELECT `ID`,`UserName`,`LastName`,`FirstName`,`Rank` from Users WHERE ID = $ID";
        break;
      case 'Co':
        ContractEditor($ID); 
        break;
      case 'Cu':
        $SQl = "SELECT * from Customers WHERE ID = $ID"; 
        break;
      case 'Ma':
        $SQl = "SELECT * from Materials WHERE ID = $ID"; 
        break;
      case 'Wo':
        $SQl = "SELECT * from Work WHERE ID = $ID";  
        break;
    }

    if($Parametr != 'Co') {
      $Input = ReturnDatabase($SQl);
      Formator( $Input[0], $Parametr, $ID); // Create form
    }
  }

  function ContractEditor($ID) {
    $SQl = "Select * from Contract WHERE ID = $ID";
    $Constract = ReturnDatabase($SQl);

    $CusID = $Constract[0]["CusID"];
    $UserID = $Constract[0]["UserID"];

    $Price = $Constract[0]["Price"];
    $Day = $Constract[0]["Day"];

    $SQl = "Select ID,UserName from Users";
    $Users = ReturnDatabase($SQl);

    $SQl = "Select ID, LastName from Customers";
    $Customers = ReturnDatabase($SQl);
    
    echo "<div class='form-container'>";
      echo '<form action="index.php" method="get">';
        echo ('<input name="ID" value='.$ID.' READONLY>');

        echo "<select name='CusID'>";
          foreach ($Customers as $Custumer) {
            if ($Custumer["ID"] == $CusID) {
              echo ('<option value='.$Custumer["ID"].' SELECTED>'.$Custumer["LastName"].'</option>');
            }
            else{
              echo ('<option value='.$Custumer["ID"].'>'.$Custumer["LastName"].'</option>');
            }
          } 
        echo "</select>";

        echo "<select name='UserID'>";
          foreach ($Users as $User) {
            if ($User["ID"] == $UserID) {
              echo ('<option value='.$User["ID"].' SELECTED>'.$User["UserName"].'</option>');
            }
            else{
              echo ('<option value='.$User["ID"].'>'.$User["UserName"].'</option>');
            }
          } 
        echo "</select>";

        echo ('<input name="Price" value='.$Price.' type="number" READONLY>');
        echo ('<input name="Day" value='.$Day.' type="Date">');


        echo '<button name="changer" type="button" onClick="Changer(0,1)">Materiály</button>';
        echo '<button name="changer" type="button" onClick="Changer(1,1)">Druhy Prací</button>';
        echo '<button name="contractUpdate" type="submit">Poslat</button>';
        echo '<button name="pdf" type="submit">Náhled</button>';
        echo '<button name="contractDelete" value='.$ID.' type="submit">Smazat</button>';
      echo "</form>";
      echo '<div id="contract"></div>';
    echo "</div>";  
  }
?>