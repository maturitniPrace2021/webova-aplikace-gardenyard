<?php 
  require_once("../connection/connect.php");

  function FormCreator($ConID){
    $SQl = "SELECT * FROM Materials";
    $Materials = ReturnDatabase($SQl);
    $SQl = "SELECT * FROM MaterialList WHERE ConID = $ConID";
    $Materials_Checked = ReturnDatabase($SQl);

    $SQl = "SELECT * FROM Work";
    $Works = ReturnDatabase($SQl);
    $SQl = "SELECT * FROM WorkList WHERE ConID = $ConID";
    $Works_Checked = ReturnDatabase($SQl);

    $Filtr_Materials = DataChanger($Materials,$Materials_Checked, "Materials");
    $Filtr_Works = DataChanger($Works,$Works_Checked, "Work");
    
    $Print = [ $Filtr_Materials, $Filtr_Works];
    echo( json_encode($Print));
    //print_r ($Print);
  }
  
  function DataChanger( $AllData,$Data_Checked, $Type){
    $Change = [];
    $Merge = [];
    
    for ($i=0; $i < count($AllData); $i++) {
      $default = ["Amount"=>0];
      $Merge[] = array_merge( $AllData[$i], $default); 
    }

    if ($Type == "Materials") {
      $ID = "MatID";
    }

    if ($Type == "Work") {
      $ID = "WorkID";
    }

    foreach ($Merge as $link) {
      for ($i=0; $i < count($Data_Checked); $i++) { 
        if ($Data_Checked[$i][$ID] == $link["ID"]) {
          $link["Amount"] = $Data_Checked[$i]["Amount"];
        }
      }
      $Change[] = $link;
    } 

    return  $Change;
  }

  function MaterialInfo($ID){
    $SQl = "SELECT ID,Price FROM Materials WHERE ID = $ID";
    $Material = ReturnDatabase($SQl);
    echo( json_encode($Material));
  }

  function contractAmountM($ID){
    $SQl = "SELECT ID, Price FROM Materials WHERE ID = $ID";
    $Material = ReturnDatabase($SQl);
    echo( json_encode($Material));
  }

  function contractAmountW($ID){
    $SQl = "SELECT ID, Rate FROM Work WHERE ID = $ID";
    $Material = ReturnDatabase($SQl);
    echo( json_encode($Material));
  }

  function Grapf($Year){
    for ($i=1 ; $i <  13; $i++ ) { 
      $SQl = "SELECT COUNT(ID) FROM `Contract` WHERE MONTH(Day) = $i AND YEAR(Day) = $Year";
      $Data[] = (array) ReturnDatabase($SQl);
    }

    foreach ($Data as $Month) {
      $Months[] = ($Month[0]['COUNT(ID)']);
    }

    echo( json_encode($Months));
  }

  function Year($Year){
    if ($_COOKIE['Rank'] == 'admin') {
      $SQLP = "SELECT SUM(Price) FROM Contract WHERE Year(Day) = ".$Year;
      $SQLC = "SELECT COUNT(ID) FROM Contract WHERE Year(Day) = ".$Year;
    }
    else {
      $ID = $_COOKIE['ID'];
      
      $SQLP = "SELECT SUM(Price) FROM Contract WHERE UserID = $ID AND Year(Day) = ".$_POST['year'];
      $SQLC = "SELECT COUNT(ID) FROM Contract WHERE UserID = $ID AND Year(Day) = ".$_POST['year'];
    }
    
    $Price = (array) ReturnDatabase($SQLP);
    $Count = (array) ReturnDatabase($SQLC);

    $Data = [$Count[0]['COUNT(ID)'], $Price[0]['SUM(Price)']];

    echo( json_encode($Data));
  }  

  if (isset($_GET['contract'])) {
    FormCreator($_GET['contract']);
  }

  if (isset($_GET['amountM'])) {
    contractAmountM($_GET['amountM']);
  }

  if (isset($_GET['amountW'])) {
    contractAmountW($_GET['amountW']);
  }

  if (isset($_GET['mat'])) {
    MaterialInfo($_GET['mat']);
  }

  if (isset($_GET['table']) && isset($_GET['page'])) {
    Pagation($_GET['page'], $_GET['table']);
  }
  
  if (isset($_GET['graph'])) {
    Grapf($_GET['graph']);
  }

  if (isset($_GET['year'])) {
    Year($_GET['year']);
  }
?>

     