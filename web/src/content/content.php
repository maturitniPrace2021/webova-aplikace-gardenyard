<?php 
  require_once('creators.php');
  require_once('../src/connection/connect.php');
  require_once('../vendor/latte/src/latte.php');

  // Form creator section

  // Function: get create-form files or dashboard site

  function getContent($FileName){
      
    $latte = new Latte\Engine;

    $latte->setTempDirectory('../vendor/latte/temp');

    if ($FileName == 'dashboard') {
      if ($_COOKIE['Rank'] == 'admin') {
        $SQLY = "SELECT DISTINCT Year(Day) AS Year FROM Contract";

        if (isset($_POST['yearChanger'])) {
          $SQLP = "SELECT SUM(Price) FROM Contract WHERE Year(Day) = ".$_POST['year'];
          $SQLC = "SELECT COUNT(ID) FROM Contract WHERE Year(Day) = ".$_POST['year'];
         }
        else {
          $ActualYear = date("Y");

          $SQLP = "SELECT SUM(Price) FROM Contract WHERE Year(Day) = $ActualYear";
          $SQLC = "SELECT COUNT(ID) FROM Contract WHERE Year(Day) = $ActualYear";
        }
      }
      else {
        $ID = $_COOKIE['ID'];
        
        $SQLY = "SELECT DISTINCT Year(Day) AS Year FROM Contract WHERE UserID = $ID";

        if (isset($_POST['year'])) {
          $SQLP = "SELECT SUM(Price) FROM Contract WHERE UserID = $ID AND Year(Day) = ".$_POST['year'];
          $SQLC = "SELECT COUNT(ID) FROM Contract WHERE UserID = $ID AND Year(Day) = ".$_POST['year'];
         }
        else {
          $ActualYear = date("Y");

          $SQLP = "SELECT SUM(Price) FROM Contract WHERE UserID = $ID AND Year(Day) = $ActualYear";
          $SQLC = "SELECT COUNT(ID) FROM Contract WHERE UserID = $ID AND Year(Day) = $ActualYear";
        }
      }
    
      $years = ReturnDatabase($SQLY);
      if(empty($years)){$yearsV[0] = 0;}
      else {
        foreach ($years as $year) {
          if ($year['Year'] == 0) {
            unset($year['Year']);
          }
          else {
            $yearsV[] = ($year['Year']); 
          }
        }
      }

    
      //získání hodnot pro boxíky
      $earned = ReturnDatabase($SQLP);
      if(empty($earned)){$years[0] = array('null' => 0);}
      $earned = array_values($earned[0]); //vydělené peníze
    
      $contracts = ReturnDatabase($SQLC);//počet zakázek
      if(empty($contracts)){$years[0] = array('null' => 0);}
      $contracts = array_values($contracts[0]);
    
      if ($earned[0] == NULL) {
        $earned[0] = 0;
      }
    
      $params = [
        'Years'=> $yearsV,  
        'Totalearned'=> $earned[0],
        'Totalcontract'=> $contracts[0],
      ];

      $latte->render("../dashboard/assets/templates/latte/".$FileName.".latte", $params); 
    }
    elseif ($FileName == 'contractAdd') {
      $SQL = "SELECT ID, UserName FROM Users";
      $Users = ReturnDatabase($SQL);
    
      $SQL = "SELECT ID, FirstName, Lastname FROM Customers";
      $Customers = ReturnDatabase($SQL);
    
      $params = [
        "Users"=>  $Users,
        "Customers"=> $Customers
      ];
    
      $latte->render("../dashboard/assets/templates/latte/".$FileName.".latte", $params); 
    }
    else {
      $latte->render("../dashboard/assets/templates/latte/".$FileName.".latte");
    }   
  }

  // Function: Singpost to forms

  function Singpost($Content) {
    switch ($Content){
      case 1: 
        $Category = 'userAdd';
        break;
      case 2: 
        $Category = 'customersAdd';
        break;
      case 3: 
        $Category = 'contractAdd';
        break;
      case 4: 
        $Category = 'materialAdd';
        break;
      case 5: 
        $Category = 'workAdd';
        break;
      default: $Category = 'dashboard';
    }

    getContent($Category);
  }

  // Create view of chose category section

  // Users, Materials, Works list, Customers
    function getView($Content, $Page, $Filtr){
      
      switch ($Content){
        case 1: 
          $Table = 'Users';
          $Parametr = 'Us';
          break;
        case 2: 
          $Table = 'Customers';
          $Parametr = 'Cu';
          break;
        case 3: 
          $Table = 'Contracts';
          $Parametr = 'Co';
          break;
        case 4: 
          $Table = 'Materials';
          $Parametr = 'Ma';
          break;
        case 5: 
          $Table = 'Work';
          $Parametr = 'Wo';
          break;
      }
   
      Pagation($Table, $Parametr, $Page, $Filtr);
    }

  // Function: Table pagation 

  function Pagation($Table, $Parametr, $Page, $Filtr){
    $Limit = 10;   

    if ($Page == 1) {
      $page_first_result = 0 * $Limit;
    }
    else {
      $page_first_result = ($Page-1) * $Limit;
    }

    if ($Table == 'Users') {
      $SQl = "SELECT ID,UserName,LastName,FirstName,Rank FROM Users LIMIT $Limit OFFSET ".$page_first_result;
    }
    elseif ($Table == 'Contracts') {
      if ($_COOKIE['Rank'] == 'admin') {  
        $SQl = "SELECT Contract.ID,Users.UserName,Customers.LastName, Contract.Price, Contract.Day FROM Contract JOIN Users ON Contract.UserID = Users.ID JOIN Customers On Contract.CusID = Customers.ID
        WHERE  Contract.Status = $Filtr
        ORDER BY Contract.ID LIMIT $Limit OFFSET $page_first_result";
      }
      else {
        $ID = $_COOKIE['ID'];
        $SQl = "SELECT Contract.ID,Users.UserName,Customers.LastName, Contract.Price, Contract.Day FROM Contract JOIN Users ON Contract.UserID = Users.ID JOIN Customers On Contract.CusID = Customers.ID
        WHERE  Contract.Status = $Filtr AND Contract.UserID = $ID
        ORDER BY Contract.ID LIMIT $Limit OFFSET $page_first_result";
      }
    }
    else {
      $SQl = "SELECT * FROM $Table LIMIT $Limit OFFSET $page_first_result";
    }

    $Data = ReturnDatabase($SQl);
    
    if ($Table == 'Contracts') {
      TablatorC($Parametr, $Data, $Page, $Filtr);
    }
    else {
      Tablator($Parametr, $Data, $Page);
    }
  }  

  //Content conditionals

  if (isset($_GET['content']) && isset($_GET['type'])) {
    if (!isset($_GET['page'])) {
      $Content = $_GET['content'];
      Singpost($Content);
    }
    elseif (isset($_GET['page'])  && isset($_GET['filtr'])) {
      $Content = $_GET['content'];
      $Page = $_GET['page'];
      $Filtr = $_GET['filtr'];

      getView($Content, $Page, $Filtr);
    }
    elseif (isset($_GET['page'])) {
      $Content = $_GET['content'];
      $Page = $_GET['page'];

      getView($Content, $Page, 0);
    }
    else {getContent('dashboard');}
  }
  elseif (isset($_GET['edit'])) { Editor($_GET['edit']);}
  else {getContent('dashboard', 0);}
?>
