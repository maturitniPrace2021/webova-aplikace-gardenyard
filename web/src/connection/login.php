<?php 
  require_once('connect.php'); // Include server-call API

  if (isset($_POST['login-name']) && isset($_POST['login-pswd'])) {
    
    $Username = htmlspecialchars($_POST['login-name']); 
    $Password = htmlspecialchars($_POST['login-pswd']); // Protect values

    Access($Username, $Password); // Call fce for check values with database
  }
  
  function Access($Login, $PSWD){
    
    $LoginID = "'$Login'"; // Login to string
    $PSWD_Hash =   "'".sha1($PSWD)."'"; // Password to string and hashing password

    $SqlCommand = "SELECT * FROM Users WHERE UserName = $LoginID AND Password = $PSWD_Hash"; // SQL Command

    $Database = ReturnDatabase($SqlCommand); // Get results

    if(isset($Database) && !empty($Database)){
      if($LoginID = $Database[0]["UserName"] && (sha1($PSWD) == $Database[0]["Password"])){

        $expiration= time()+60*60*24*30; // Set expiration on 30 days

        setcookie ('ID', $Database[0]["ID"], $expiration); //Create cookie
        setcookie ('Rank', $Database[0]["Rank"], $expiration); //Create cookie

        header("Location:dashboard/"); // Send to dashboard page
      }
    }
  }
?>