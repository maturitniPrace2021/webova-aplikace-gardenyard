<?php   

  //Define atributs for connetion to Database 
  
  define ("DBSERVERNAME"," "); // Database URL
  define ("DBUSERNAME"," ");   // Database username
  define ("DBPASSWORD"," "); // Database password
  define ("DBNAME"," "); // Database name

  function ReturnDatabase($Sql){
    $Data;
    
    $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME); // Database connection
    
    if ($conn->connect_error) { // Fatal connection protect
      die("Connection failed: " . $conn->connect_error); 
    }
    
    $result = $conn->query($Sql); // Push SQL command

    if ( isset($result->num_rows) && $result->num_rows > 0) {
      while($row = $result->fetch_assoc()) { 
        $Data[] = $row; // Get value from database
      }
    }

    if(isset($Data)){return($Data);} // Return data
  }

  function pushCommandToDatabase($Sql_Command){
  
    $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME); // Database connection
    
    if ($conn->connect_error) { // Fatal connection protect
      die("Connection failed: " . $conn->connect_error);
    }
  
    $Sql = $Sql_Command; // Set SQL command
      
    $Push = $conn->query($Sql); // Send SQL command to database
  }
  ?>