<?php 
  require_once("../src/database_Api/databaseApi.php");

  const Currency = ' Kč';

  if (isset($_POST['btnUseradd'])) {
    if (isset($_POST['username'])&&isset($_POST['password'])&&isset($_POST['rank'])) {
      $username = $_POST['username'];
      $firstname = $_POST['firstname'];
      $lastname = $_POST['lastname'];
      $password = $_POST['password'];
      $rank = $_POST['rank'];

      insertUser($username,$lastname,$firstname,$password,$rank);
    }
  }

  if (isset($_POST['btnMaterialadd'])) {
    if (isset($_POST['description'])&&isset($_POST['price'])) {
      $index = $_POST['index'];
      $description = $_POST['description'];
      $Price = $_POST['price'];

      $Price = strval( filter_var( $Price, FILTER_SANITIZE_NUMBER_INT));
      $Rate = "$Price" . Currency;
      
      insertMaterial($index, $description, $Rate);
    }
  }

  if (isset($_POST['btnWorkadd'])) {
    if (isset($_POST['description'])&&isset($_POST['price'])) {
      $Type = $_POST['description'];
      $Price = $_POST['price'];
      
      $Price = strval( filter_var( $Price, FILTER_SANITIZE_NUMBER_INT));
      $Rate = "$Price" . Currency;
      
      insertWorks($Type, $Rate);
    }
  }

 if (isset($_GET["contractbtn"])) {
    if (isset($_GET["date"]) && isset($_GET["customers"]) && isset($_GET["users"]) ){

      $Date = $_GET["date"];
      $Customer = $_GET["customers"];
      $User = $_GET["users"];

      insertContract($Customer, $User,$Date);
    }
 } 
 
  if (isset($_POST["btnCustomerladd"])) {
    if (isset($_POST["lastname"]) && isset($_POST["addres"])){
      $FirstName = $_POST["firstname"];
      $LastName = $_POST["lastname"];
      $Address = $_POST["addres"];
      $Email = $_POST["email"];
      $Tel = $_POST["tel"];

      insertCustomer($FirstName, $LastName,$Tel,$Email,$Address);
    
    }
  }       
    
  if (isset($_GET["contractUpdate"])) {
    if (isset($_GET["ID"]) && isset($_GET["CusID"])){
      $ID = $_GET["ID"];
      $CusID = $_GET["CusID"];
      $UserID = $_GET["UserID"];
      $Day = $_GET["Day"];
      $Price = $_GET["Price"];

      updateContract($CusID, $UserID,$Day, $ID, $Price, 1);
    }
  }

  if (isset($_POST["customersUpdate"])) {
    if (isset($_POST["ID"]) && isset($_POST["LastName"])){
      $ID = $_POST["ID"];
      $FirstName = $_POST["FirstName"];
      $LastName = $_POST["LastName"];
      $Tel = $_POST["Tel"];
      $Email = $_POST["Email"];
      $Address = $_POST["Address"];

      
      updateCustomer($ID,$FirstName, $LastName,$Tel,$Email,$Address);
    }
  }

  if (isset($_POST["usersUpdate"])) {
    if (isset($_POST["ID"]) && isset($_POST["UserName"]) && isset($_POST["Rank"])){    
      $ID = $_POST["ID"];
      $UserName = $_POST["UserName"];
      $LastName = $_POST["LastName"];
      $FirstName = $_POST["FirstName"];
      $Rank = $_POST["Rank"];
      
      updateUser($ID, $UserName, $LastName, $FirstName, $Rank);
    }
  }

  if (isset($_POST["materialsUpdate"])) {
    if (isset($_POST["ID"]) && isset($_POST["Description"]) && isset($_POST["Price"])){
      $ID = $_POST["ID"];
      $Index = $_POST["Index"];
      $Description = $_POST["Description"];
      $Price = $_POST["Price"];

      updateMaterial($ID,$Index, $Description,$Price);
    }
  }

  if (isset($_POST["workUpdate"])) {
    if (isset($_POST["ID"]) && isset($_POST["Type"]) && isset($_POST["Rate"])){ 
      $ID = $_POST["ID"];
      $Type = $_POST["Type"];
      $Rate = $_POST["Rate"];

      updateWorks($ID,$Type, $Rate);
    }
  }

// Delete section 

if(isset($_POST["userDelete"])) {

  $ID = $_POST["userDelete"];

  deleteUser($ID);
}

if(isset($_POST["customerDelete"])) {

  $ID = $_POST["customerDelete"];

  deleteCustomer($ID);
}

if(isset($_POST["materialDelete"])) {

  $ID = $_POST["materialDelete"];

  deleteMaterial($ID);
}

if(isset($_POST["contractDelete"])) {

  $ID = $_POST["contractDelete"];

  deleteContract($ID);
}

if(isset($_POST["workDelete"])) {

  $ID = $_POST["workDelete"];
  
  deleteWork($ID);
}
?>