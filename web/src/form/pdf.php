<?php 
  require_once('vendor/fpdf/fpdf.php'); // Include PDF Api

  if (isset($_GET['pdf'])) { 
    $ID = $_GET['ID']; // Contract ID
    $CusID = $_GET['CusID']; // Customer ID
    $UserID = $_GET['UserID']; // User ID
    $Price = $_GET['Price']; // Price value
    $Day = $_GET['Day']; // Time Value

    class PDF extends FPDF {

      function Creator($Day, $ID, $Price) { // Function to create pdf values

        $Keywords = ['Zakázka', 'Cena za práci:', 'CZK']; // Create array with czech words

        $this->SetFont('Arial','B',15); // Set font of PDF title

        $this->Cell(10); // Move the cursor
        $this->Image('assets/img/favicon-32x32.png',10, 10); // Add logo picture

        $this->Cell(5); // Change cursor position
        $this->Cell(30,10,'GardenYard', 0, 0, 'C'); // Add PDF title
        $this->Ln(20);  // Line break

        $this->SetFont('Arial','',12); // Set font of PDF text

        $this->Cell(130 ,5,'Slovany 1',0,0); // Add address column
        $this->Cell(59 ,5,'',0,1); //Change cursor position

        $this->Cell(130 ,5,'Plzen, CZ, 32600',0,0); // Add City column
        $this->Cell(25 ,5,'Datum:',0,0); // Add time column
        $this->Cell(34 ,5, $Day, 0, 1); // Add value of time

        $this->Cell(130 ,5,'Mobil: +420 123 456 789',0,0); // Add Phone column

        $this->Cell(25 ,5,  utf8_decode($Keywords[0]),0,0); // Add 'Zakázka' column
        $this->Cell(34 ,5,$ID,0,1); // Add ID value

        $this->Line(10, 55, 200, 55); // Add line 

        $this->Cell(40 ,36,utf8_decode($Keywords[1]),0,0); // Add 'Cena za práci:' column
        $this->Cell(15 ,36,$Price,0,0); //Add price value
        $this->Cell(120 ,36,utf8_decode($Keywords[2]),0,0);  //Add CZK 
      }
  }

  $pdf = new PDF(); // Create new object 
  $pdf->SetFont('Arial','',14); // Set font of PDF file 
  $pdf->SetKeywords('Zakázka' , true);
  $pdf->AddPage(); // Add page to PDF file

  $pdf->Creator($Day, $ID, $Price); // Generate text in page 

  $pdf->Output('I', 'gardenyard.php', true); // Export PDF file
}
?>