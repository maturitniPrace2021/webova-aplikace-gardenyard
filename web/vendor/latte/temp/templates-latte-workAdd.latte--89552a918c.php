<?php
// source: assets/templates/latte/workAdd.latte

use Latte\Runtime as LR;

class Template89552a918c extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<div class="marg">
  <h1>Přidat produkt </h1>

  <form action="index.php" method="post" >
    <div class="DIV-1row">
      <div class="Col" style="width:100%">
        <input type="text" name="description" placeholder="Název..">
        <input type="number" name="price" placeholder="Cena za hodinu">
        <button type="submit" name="btnWorkadd">Přidat</button>
      </div>
    </div>
  </form>
</div>
<?php
		return get_defined_vars();
	}

}
