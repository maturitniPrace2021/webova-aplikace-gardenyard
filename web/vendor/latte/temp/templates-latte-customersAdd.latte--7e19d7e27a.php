<?php
// source: assets/templates/latte/customersAdd.latte

use Latte\Runtime as LR;

class Template7e19d7e27a extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<div class="marg">
  <h1>Přidat zákazníka</h1>
  <form class="" action="index.php" method="post">
    <div class="DIV-1row">
      <div class="Col">
        <input type="text" name="firstname" placeholder="jméno...">
        <input type="text" name="lastname" placeholder="přijmění...">
        <input type="text" name="addres" placeholder="ulice...">
      </div>
      <div class="Col">
        <input type="tel" name="tel" placeholder="telefon...">
        <input type="email" name="email" placeholder="email...">
        <button type="submit" name="btnCustomerladd">PŘIDAT</button>
      </div>
    </div>
  </form>
</div>

<?php
		return get_defined_vars();
	}

}
