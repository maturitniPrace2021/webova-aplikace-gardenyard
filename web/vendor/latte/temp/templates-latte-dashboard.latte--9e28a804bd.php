<?php
// source: assets/templates/latte/dashboard.latte

use Latte\Runtime as LR;

class Template9e28a804bd extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<section>
  <div class="content">
    <h2>Přehled</h2>
    <div class="sum-div">
      <div class="sum-inputs">
<?php
		$iterations = 0;
		foreach ($Years as $value) {
			?>              <a href="index.php?year=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($value)) /* line 7 */ ?>"><?php
			echo LR\Filters::escapeHtmlText($value) /* line 7 */ ?></a>
<?php
			$iterations++;
		}
?>
      </div>

      <div class="summary-box">
        <div class="summary-but">
          <p>Celkem vyděláno</p>
          <p class="summary-but-n"><?php echo LR\Filters::escapeHtmlText($Totalearned) /* line 14 */ ?></p>
          <p>Kč</p>
        </div>
        <div class="summary-but">
          <p>Celkový počet</p>
          <p class="summary-but-n"><?php echo LR\Filters::escapeHtmlText($Totalcontract) /* line 19 */ ?></p>
          <p>Zakázek</p>
        </div>
      </div>
    </div>
  </div>
  <div class="graph">
    <canvas id="graphAPI" width="400" height="100" aria-label="Hello ARIA World" role="img"></canvas>
  </div>
</section>

<script src="assets/js/graph.js"></script>
<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['value'])) trigger_error('Variable $value overwritten in foreach on line 6');
		
	}

}
