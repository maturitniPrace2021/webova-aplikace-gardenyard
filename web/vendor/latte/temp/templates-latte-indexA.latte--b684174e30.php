<?php
// source: assets/templates/latte/indexA.latte

use Latte\Runtime as LR;

class Templateb684174e30 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html lang="cz">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GardenYard</title>
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
    <link rel="stylesheet" href="assets/css/main.css">
  </head>
  <body>
    <main>
<?php
		require_once("assets/templates/sidebar/sidebar.html");
		;
		require_once("../src/connection/verification.php");
		;
?>
      <div class="content">
          <header>
            <button id="responsivebtn" class="view">
              <div class="button">
                <div></div>
              </div>
            </button>
          </header>
          <div class="content-main">
<?php
		require_once("../src/content/content.php");
		;
?>
          </div>
      </div>
    </main>
  </body>
</html><?php
		return get_defined_vars();
	}

}
