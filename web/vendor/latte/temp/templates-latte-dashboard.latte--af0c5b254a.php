<?php
// source: ../dashboard/assets/templates/latte/dashboard.latte

use Latte\Runtime as LR;

class Templateaf0c5b254a extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<section>
  <div class="content">
    <h2>Přehled</h2>
    <div class="sum-div">
      <div class="sum-inputs">
<?php
		if (($_COOKIE['Rank'] == 'admin')) {
?>
          <form action="index.php" method="post">
            <select  name="year">
<?php
			$iterations = 0;
			foreach ($Years as $value) {
				?>                    <option value="<?php echo LR\Filters::escapeHtmlAttr($value) /* line 10 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($value) /* line 10 */ ?></option>
<?php
				$iterations++;
			}
?>
            </select>
            <button type="submit" name="yearChanger">Graf</button>
          </form>
<?php
		}
?>
      </div>

      <div class="summary-box">
        <div class="summary-but">
          <p>Celkem vyděláno</p>
          <p class="summary-but-n"><?php echo LR\Filters::escapeHtmlText($Totalearned) /* line 21 */ ?></p>
          <p>Kč</p>
        </div>
        <div class="summary-but">
          <p>Celkový počet</p>
          <p class="summary-but-n"><?php echo LR\Filters::escapeHtmlText($Totalcontract) /* line 26 */ ?></p>
          <p>Zakázek</p>
        </div>
      </div>
    </div>
  </div>
<?php
		if (($_COOKIE['Rank'] == 'admin')) {
?>
    <div class="graph">
      <canvas id="graphAPI" width="400" height="100" aria-label="Hello ARIA World" role="img"></canvas>
    </div>
<?php
		}
?>
</section>

<script src="assets/js/graph.js"></script>
<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['value'])) trigger_error('Variable $value overwritten in foreach on line 9');
		
	}

}
