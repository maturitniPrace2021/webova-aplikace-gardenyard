<?php
// source: assets/templates/latte/index.latte

use Latte\Runtime as LR;

class Template6a1e2ffe4d extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html lang="cz">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GardenYard</title>
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
    <link rel="stylesheet" href="assets/css/main.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
  <script src='https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js' ></script>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
  </head>
  <body>
    <main>
<?php
		require_once("../src/connection/verification.php");
		;
		include_once("../src/form/form.php");
		;
?>
      
<?php
		if($_COOKIE['Rank'] == 'admin') {
			require_once("assets/templates/sidebar/sidebar.html");
		}
		else {
			require_once("assets/templates/sidebar/sidebar2.html");
		};
?>

      <div class="content">
          <header>
            <button id="responsivebtn" class="view">
              <div class="button">
                <div></div>
              </div>
            </button>
          </header>
          <div class="content-main">
<?php
		require_once("../src/content/content.php");
		;
?>
          </div>
      </div>
    </main>
    
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="assets/js/sidebar.js"></script>
    <script src="assets/js/tabletor_check.js"></script>
    <script src="assets/js/calc.js"></script>
    <script src="assets/js/checker.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </body>
</html><?php
		return get_defined_vars();
	}

}
