<?php
// source: ../dashboard/assets/templates/latte/workAdd.latte

use Latte\Runtime as LR;

class Template2bc1308b8a extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<div class="marg">
  <h1>Přidat produkt </h1>

  <form action="index.php" method="post" >
    <div class="content-div">
      <div class="Col" style="width:100%">
        <input type="text" name="description" placeholder="Název..">
        <input type="number" name="price" placeholder="Cena za hodinu">
        <button type="submit" name="btnWorkadd">Přidat</button>
      </div>
    </div>
  </form>
</div>
<?php
		return get_defined_vars();
	}

}
