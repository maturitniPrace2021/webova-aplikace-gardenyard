-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Počítač: innodb.endora.cz:3306
-- Vytvořeno: Pát 16. dub 2021, 21:20
-- Verze serveru: 5.6.45-86.1
-- Verze PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `worksheetdb`
--
CREATE DATABASE IF NOT EXISTS `worksheetdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `worksheetdb`;

-- --------------------------------------------------------

--
-- Struktura tabulky `Contract`
--

CREATE TABLE `Contract` (
  `ID` int(11) NOT NULL,
  `UserID` int(250) DEFAULT NULL,
  `CusID` int(250) DEFAULT NULL,
  `Price` int(250) NOT NULL,
  `Day` date DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `Contract`
--

INSERT INTO `Contract` (`ID`, `UserID`, `CusID`, `Price`, `Day`, `Status`) VALUES
(2, 4, 5, 5721, '2002-06-01', 1),
(3, 2, 2, 0, '2021-04-16', 0),
(4, 3, 4, 10210, '2000-01-06', 1),
(5, 4, 11, 0, '2021-07-21', 0),
(6, 1, 7, 0, '2021-11-30', 0),
(7, 1, 1, 0, '2000-01-19', 0),
(8, 3, 4, 0, '2000-01-30', 0),
(9, 2, 5, 0, '2021-04-16', 0),
(10, 1, 11, 0, '2000-01-01', 0),
(11, 4, 10, 0, '2000-01-01', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `Customers`
--

CREATE TABLE `Customers` (
  `ID` int(11) NOT NULL,
  `FirstName` char(100) DEFAULT NULL,
  `LastName` char(100) NOT NULL,
  `Tel` char(100) DEFAULT NULL,
  `Email` char(100) DEFAULT NULL,
  `Address` char(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `Customers`
--

INSERT INTO `Customers` (`ID`, `FirstName`, `LastName`, `Tel`, `Email`, `Address`) VALUES
(2, 'Petra', 'Adámková', '605821336', '', 'Adamova 19'),
(3, 'Alan', 'Folmava', '777211296', '', 'Folmava 90'),
(4, 'Eliška', 'Antonová', '737475605', '', 'Bílá Hora'),
(5, 'Tomáš', 'Baťa', '360560457', '', 'Zlín'),
(6, 'Steave', 'Jobs', '365475880', '', 'Sillicon Valey'),
(7, 'ARC Heating', 'ARC Heating', '603552048', NULL, 'Plzeň'),
(8, 'Tonda', 'Drnek', '603486247', NULL, 'Bručná'),
(9, 'Fanda', 'Hrnek', '607979461', NULL, 'Bručná'),
(10, NULL, 'Ekonomické Stavby', '602256839', NULL, 'Chotíkov'),
(11, NULL, 'Fakultní nemocnice', '602324005', NULL, 'Lochotín'),
(12, 'Filip', 'Hrbek', '682487451', 'filip.hrbek@email.com', 'K Bukové 18');

-- --------------------------------------------------------

--
-- Struktura tabulky `MaterialList`
--

CREATE TABLE `MaterialList` (
  `ID` int(11) NOT NULL,
  `ConID` int(11) NOT NULL,
  `MatID` int(11) NOT NULL,
  `Amount` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `MaterialList`
--

INSERT INTO `MaterialList` (`ID`, `ConID`, `MatID`, `Amount`) VALUES
(1, 4, 1, '4'),
(3, 6, 2, '10');

-- --------------------------------------------------------

--
-- Struktura tabulky `Materials`
--

CREATE TABLE `Materials` (
  `ID` int(11) NOT NULL,
  `Index` char(100) NOT NULL,
  `Description` char(100) NOT NULL,
  `Price` char(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `Materials`
--

INSERT INTO `Materials` (`ID`, `Index`, `Description`, `Price`) VALUES
(1, 'RB', 'JTV elmag. ventil 1', '1675,00 Kč'),
(2, 'RB', 'MTT-100 T-kus', '97,00 Kč'),
(3, 'RB', 'MTT-100 koleno', '860,00 Kč'),
(4, 'RB', 'DBRY-6 vod. konektor', '75,00 Kč'),
(5, 'RB', 'SNAPLOC BVS-1', '36,00 Kč'),
(6, 'RB', 'VBA šachtice s KK3/4', '645,00 Kč'),
(7, 'RB', 'Postřik. 1804 -SAM', '100,00 Kč'),
(8, 'RB', 'Tryska rot. R-VAN', '299,00 Kč'),
(9, 'RB', 'Tryska rozp. VAN', '89,00 Kč'),
(10, 'RB', 'Tryska rozp. HE-VAN', '80,00 Kč'),
(11, 'RB', 'Rot. postř. 3504', '653,00 Kč'),
(12, 'RB', 'Rot. post. 5004', '647,00 Kč'),
(13, 'RB', 'SBE-050 tvarovka', '19,30 Kč'),
(14, 'RB', 'BLAZING 32 X SP100', '66,50 Kč'),
(15, 'RB', 'SPX Flex tubing', '37,50 Kč'),
(16, 'RB', 'PSI M50 reg. tlaku', '617,00 Kč'),
(17, 'RB', 'Spojka 1/4 spaghetti', '125,00 Kč'),
(18, 'PE', 'Trubka 25x2 PN7,5', '19,00 Kč'),
(19, 'PE', 'Trubka 32x2 PN7,5', '25,00 Kč'),
(20, 'PE', 'Navrt. pas 25x1/2', '19,00 Kč'),
(21, 'PE', 'Navrt. pas 32x1/2', '23,00 Kč'),
(22, 'PE', 'Spojka přímá 25', '44,00 Kč'),
(23, 'PE', 'Spojka přímá 32', '58,00 Kč'),
(24, 'PE', 'Redukce 25x1 AG', '27,00 Kč'),
(25, 'PE', 'Redukce 32x1 AG', '34,00 Kč'),
(26, 'PE', 'Redukce 25x3/4 IG', '29,00 Kč'),
(27, 'PE', 'Redukce 32x1 IG', '41,00 Kč'),
(28, 'PE', 'Redukce 32x25', '59,00 Kč'),
(29, 'PE', 'Koleno 25', '44,00 Kč'),
(30, 'PE', 'Koleno 32x1 AG', '42,00 Kč'),
(31, 'PE', 'T-kus 25', '63,00 Kč'),
(32, 'PE', 'T-kus 32', '94,00 Kč'),
(33, 'PPR', 'Trubka 20 PN16', '28,00 Kč'),
(34, 'PPR', 'Trubka 25 PN16', '42,00 Kč'),
(35, 'PPR', 'Trubka 32 PN16', '52,00 Kč'),
(36, 'PPR', 'Zástřik 20x1/2 AG', '54,00 Kč'),
(37, 'PPR', 'Zástřik 25x3/4 AG', '64,00 Kč'),
(38, 'PPR', 'Zástřik 32x1 AG', '125,00 Kč'),
(39, 'PPR', 'Zástřik 20x1/2 AG', '54,00 Kč'),
(40, 'PPR', 'Zástřik 25x3/4 AG', '64,00 Kč'),
(42, 'PE', 'T-kus 25', '63,00 Kč'),
(43, 'PE', 'T-kus 32', '94,00 Kč'),
(44, 'PE', 'Trubka 20 PN16', '28,00 Kč'),
(45, 'PPR', 'Trubka 25 PN16', '42,00 Kč'),
(46, 'PPR', 'Trubka 32 PN16', '52,00 Kč'),
(47, 'PPR', 'Zástřik 20x1/2 AG', '54,00 Kč'),
(49, 'RB', 'Trubka 36x1 RM', '12 Kč');

-- --------------------------------------------------------

--
-- Struktura tabulky `Users`
--

CREATE TABLE `Users` (
  `ID` int(11) NOT NULL,
  `UserName` char(100) NOT NULL,
  `LastName` char(100) DEFAULT NULL,
  `FirstName` char(100) DEFAULT NULL,
  `Password` char(100) NOT NULL,
  `Rank` char(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `Users`
--

INSERT INTO `Users` (`ID`, `UserName`, `LastName`, `FirstName`, `Password`, `Rank`) VALUES
(1, 'hrbekt', 'Hrbek', 'Tomáš', 'afea0ae07a5460f844fb6301b7d9ffcd4cc8dd7e', 'admin'),
(2, 'lopatovae', 'Lopatová', 'Eliška', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'admin'),
(3, 'dlouhas', 'dlouha', 'Sára', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'worker'),
(4, 'paidarj', 'Paidar', 'Kuba', '9c969ddf454079e3d439973bbab63ea6233e4087', 'worker'),
(5, 'admin', 'admin', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin'),
(6, 'worker', 'worker', 'worker', '10f5ebe6acb4467b71602be4c4fdb44c44fa26b6', 'worker'),
(7, 'Tomasos36', 'Paidar', 'Tomáš', '3c363836cf4e16666669a25da280a1865c2d2874', 'worker');

-- --------------------------------------------------------

--
-- Struktura tabulky `Work`
--

CREATE TABLE `Work` (
  `ID` int(11) NOT NULL,
  `Type` char(100) NOT NULL,
  `Rate` char(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `Work`
--

INSERT INTO `Work` (`ID`, `Type`, `Rate`) VALUES
(1, 'Kopání', '12 Kč'),
(2, 'Hodinová sazba - závlahy, voda', '125 Kč'),
(3, 'Hodinová sazba - údržba zeleně', '600 Kč'),
(4, 'Doprava Plzeň', '400 Kč'),
(5, 'Doprava 1km', '14 Kč'),
(6, 'Vertikutace', '15 Kč'),
(7, 'Hnojení', '50 Kč'),
(8, 'Dosetí', '60 Kč'),
(9, 'Sekání', '80 Kč'),
(10, 'Pískování trávníku', '40 Kč'),
(11, 'Chemický postřik', '60 Kč'),
(12, 'Stříhání keřů', '80 Kč'),
(13, 'Kácení', '20 Kč'),
(14, 'Pletí', '90 Kč'),
(15, 'Mulčování', '40 Kč'),
(16, 'Pokládka textilie', '15 Kč'),
(17, 'Likvidace biomateriálu', '26 Kč'),
(18, 'Výměna postřikovače', '45 Kč'),
(19, 'Oprava potrubí', '36 Kč'),
(20, 'Výměna trysek', '85 Kč'),
(21, 'Čištění filtru', '45 Kč'),
(22, 'Nastavení jednotky', '96 Kč'),
(23, 'Čištění systému', '55 Kč'),
(24, 'Oprava čerpadla', '60 Kč'),
(26, 'Sázení písku', '20 Kč');

-- --------------------------------------------------------

--
-- Struktura tabulky `WorkList`
--

CREATE TABLE `WorkList` (
  `ID` int(11) NOT NULL,
  `ConID` int(11) NOT NULL,
  `WorkID` int(11) NOT NULL,
  `Amount` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `WorkList`
--

INSERT INTO `WorkList` (`ID`, `ConID`, `WorkID`, `Amount`) VALUES
(1, 4, 4, '1'),
(2, 4, 3, '1'),
(3, 4, 1, '5');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `Contract`
--
ALTER TABLE `Contract`
  ADD PRIMARY KEY (`ID`);

--
-- Klíče pro tabulku `Customers`
--
ALTER TABLE `Customers`
  ADD PRIMARY KEY (`ID`);

--
-- Klíče pro tabulku `MaterialList`
--
ALTER TABLE `MaterialList`
  ADD PRIMARY KEY (`ID`);

--
-- Klíče pro tabulku `Materials`
--
ALTER TABLE `Materials`
  ADD PRIMARY KEY (`ID`);

--
-- Klíče pro tabulku `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`ID`);

--
-- Klíče pro tabulku `Work`
--
ALTER TABLE `Work`
  ADD PRIMARY KEY (`ID`);

--
-- Klíče pro tabulku `WorkList`
--
ALTER TABLE `WorkList`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `Contract`
--
ALTER TABLE `Contract`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pro tabulku `Customers`
--
ALTER TABLE `Customers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pro tabulku `MaterialList`
--
ALTER TABLE `MaterialList`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pro tabulku `Materials`
--
ALTER TABLE `Materials`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT pro tabulku `Users`
--
ALTER TABLE `Users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pro tabulku `Work`
--
ALTER TABLE `Work`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT pro tabulku `WorkList`
--
ALTER TABLE `WorkList`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
