# GardenYard

Webová aplikace pro zprávu prácovníků ohledně zahraníckých prací. 

## Web 

* WEB: http://gardenyard.8u.cz/
1. Admin 
  * Jméno: admin
  * Heslo: admin
2. Worker 
  * Jméno: worker
  * Heslo: worker

## Instalace
* [Less](https://choosealicense.com/licenses/mit/)
* [PHP](https://www.php.net/)
* [latte](https://latte.nette.org/cs/)

## Team
* Front-End
  1. Eliška Lopatová
* Back-End
  1. Tomáš Hrbek

## Kontakt
* [Discord](https://discord.gg/V75m6RWh)
* [Teams](https://teams.microsoft.com/l/team/19%3a2b3976ff980d49359d42fedc697039ec%40thread.tacv2/conversations?groupId=be6827eb-ef07-4270-aafb-a896c93c4fb4&tenantId=81c6ea7d-7ecf-4514-a923-150ccb6c9f6a)




